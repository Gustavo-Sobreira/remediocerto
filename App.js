import { NavigationContainer } from '@react-navigation/native'

import { useFonts } from 'expo-font'
import { LilitaOne_400Regular } from '@expo-google-fonts/lilita-one'
import { TabBarNavigation } from './src/components/routes'
import { StatusBar } from 'react-native'

export default function App() {
  let [fontsLoaded, fontError] = useFonts({
    LilitaOne_400Regular
  })

  if (!fontsLoaded && !fontError) {
    return null
  }

  return (
    <NavigationContainer>
      <StatusBar backgroundColor={'#000'} style={'#Fff'} />

      <TabBarNavigation />
    </NavigationContainer>
  )
}
