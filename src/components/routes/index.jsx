import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MaterialCommunityIcons } from '@expo/vector-icons';


import { SHome, SPerfil, SSos, SLocalizacao, SGrafico } from '../../screens';
import SCamera from '../../screens/SCamera';

const Tab = createBottomTabNavigator();

const avisos = 1;
const size = 35;

const FontAtiva = "#ffffff";
const FontInativa = "#ffffffa4";

const HomeAtiva = "#388CA6";
const HomeInativa = "#38A69D";
const HomeIconeAtivo = "medical-bag";
const HomeIconeInativo = "pill";

const LocalizacaoAtiva = "#5E3E7B";
const LocalizacaoInativa = "#7138A6";
const LocalizacaoIconeAtivo = "store-search";
const LocalizacaoIconeInativo = "store-search-outline";


const SosAtiva = "#B35050";
const SosInativa = "#FF7373";
const SosIconeAtivo = "phone-alert";
const SosIconeInativo = "phone-alert-outline";

const PerfilAtiva = "#2E5873";
const PerfilInativa = "#4D93BF";
const PerfilIconeAtivo = "account-circle";
const PerfilIconeInativo = "account-circle-outline";

const CameraAtiva = "#212C33";
const CameraInativa = "#749BB3";
const CameraIconeAtivo = "camera-party-mode";
const CameraIconeInativo = "camera-outline";

const GraficoAtiva = "#388CA6";
const GraficoInativa = "#38A69D";
const GraficoIconeAtivo = "chart-bar";
const GraficoIconeInativo = "chart-bar-stacked";



export const TabBarNavigation = () => {
     return(
          <Tab.Navigator
               screenOptions={{
                    headerShown: false,
                    tabBarShowLabel: false,
                    tabBarStyle:{
                         height: 50
                    }
               }}
          >
               <Tab.Screen 
                    name="Home" 
                    component={SHome} 
                    options={{ 
                         tabBarActiveTintColor: FontAtiva,
                         tabBarActiveBackgroundColor:HomeAtiva,

                         tabBarInactiveTintColor: FontInativa,
                         tabBarInactiveBackgroundColor: HomeInativa,

                         tabBarBadge: avisos,
                         
                         tabBarIcon: ({focused, color}) => (
                              <MaterialCommunityIcons 
                                   name={focused ? HomeIconeAtivo : HomeIconeInativo} 
                                   size={size} 
                                   color={color} 
                              />
                         )
                    }}
               />
               <Tab.Screen 
                    name="Grafico" 
                    component={SGrafico} 
                    options={{ 
                         tabBarActiveTintColor: FontAtiva,
                         tabBarActiveBackgroundColor:GraficoAtiva,

                         tabBarInactiveTintColor: FontInativa,
                         tabBarInactiveBackgroundColor: GraficoInativa,

                         tabBarBadge: avisos,
                         
                         tabBarIcon: ({focused, color}) => (
                              <MaterialCommunityIcons 
                                   name={focused ? GraficoIconeAtivo : GraficoIconeInativo} 
                                   size={size} 
                                   color={color} 
                              />
                         )
                    }}
               />

               <Tab.Screen 
                    name="Sos" 
                    component={SSos} 
                    options={{ 
                         tabBarActiveTintColor: FontAtiva,
                         tabBarActiveBackgroundColor:SosAtiva,

                         tabBarInactiveTintColor: FontInativa,
                         tabBarInactiveBackgroundColor: SosInativa,

                         tabBarIcon: ({focused, color}) => (
                              <MaterialCommunityIcons 
                                   name={focused ? SosIconeAtivo : SosIconeInativo} 
                                   size={size} 
                                   color={color} 
                              />
                         )
                    }}
               />

               

               <Tab.Screen 
                    name="Localizacao" 
                    component={SLocalizacao} 
                    options={{ 
                         tabBarActiveTintColor: FontAtiva,
                         tabBarActiveBackgroundColor:LocalizacaoAtiva,

                         tabBarInactiveTintColor: FontInativa,
                         tabBarInactiveBackgroundColor: LocalizacaoInativa,

                         tabBarIcon: ({focused, color}) => (
                              <MaterialCommunityIcons 
                                   name={focused ? LocalizacaoIconeAtivo : LocalizacaoIconeInativo} 
                                   size={size} 
                                   color={color} 
                              />
                         )
                    }}
               />

               <Tab.Screen 
                    name="Perfil" 
                    component={SPerfil} 
                    options={{ 
                         tabBarActiveTintColor: FontAtiva,
                         tabBarActiveBackgroundColor: PerfilAtiva,

                         tabBarInactiveTintColor: FontInativa,
                         tabBarInactiveBackgroundColor: PerfilInativa,

                         tabBarIcon: ({focused, color}) => (
                              <MaterialCommunityIcons 
                                   name={focused ? PerfilIconeAtivo : PerfilIconeInativo} 
                                   size={size} 
                                   color={color} 
                              />
                         )
                    }}
               />
               <Tab.Screen 
                    name="Camera" 
                    component={SCamera} 
                    options={{ 
                         tabBarActiveTintColor: FontAtiva,
                         tabBarActiveBackgroundColor: PerfilAtiva,

                         tabBarInactiveTintColor: FontInativa,
                         tabBarInactiveBackgroundColor: CameraInativa,

                         tabBarIcon: ({focused, color}) => (
                              <MaterialCommunityIcons 
                                   name={focused ? CameraIconeAtivo : CameraIconeInativo} 
                                   size={size} 
                                   color={color} 
                              />
                         )
                    }}
               />          
          </Tab.Navigator>
     )
}