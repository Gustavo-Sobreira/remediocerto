
import { Alert } from 'react-native'
import * as ImagePicker from 'expo-image-picker' 
import MedicamentoRepositorySingleton from './../../database/sqlite/MedicamentoRepository'

const medicamentoRepo = MedicamentoRepositorySingleton.getInstance()

export const selecionarImagem = async nome => {
  try {
    let permissionResult = await ImagePicker.requestMediaLibraryPermissionsAsync()

    if (permissionResult.granted === false) {
      Alert.alert('Permissão negada', 'A permissão para acessar a galeria foi negada.')
      return
    }

    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1
    })

    if (!result.cancelled) {
      await medicamentoRepo.editarMedicamento(nome, result.assets[0].uri)
    }
  } catch (error) {
    console.error('Erro ao selecionar imagem:', error)
  }
}
