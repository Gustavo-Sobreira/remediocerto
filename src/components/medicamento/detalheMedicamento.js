import React from 'react';
import { Image, View } from 'react-native'
import { CTouchableOpacity, CView } from '../containers'
import { H3, H5 } from '../texto'
import { selecionarImagem } from './imagens'

export const detalhesMedicamento = ({ medicamento, navigation }) => {
  return (
    <CView
      bkColor={'transparent'}
      height={'250px'}
      elevation={'0'}
      radius={'0px'}
      mgTop={'0px'}
      mgBtn={'0px'}
      padding={'0px'}
      direction={'column'}
      align={'start'}
      justify={'start'}
    >
      <H3 cor={medicamento.quantidade >= 5 ? medicamento.cor_fonte : '#F24141'}>
        {medicamento.nome}
      </H3>
      <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
        {medicamento.uri === null ? (
          <Image
            style={{ width: 160, height: 200, backgroundColor: 'grey' }}
            source={require('../../../assets/sem_foto.png')}
          />
        ) : (
          <Image
            key={medicamento.uri}
            style={{ width: 160, height: 200, backgroundColor: 'grey' }}
            source={{ uri: medicamento.uri }}
          />
        )}
        
        <View style={{ flexDirection: 'column', justifyContent: 'space-around' }}>
          <CTouchableOpacity
            bkColor={'#EDD921'}
            elevation={'0'}
            width={'150px'}
            height={'50px'}
            radius={'100px'}
            mgTop={'0px'}
            mgBtn={'0px'}
            padding={'0px'}
            direction={'column'}
            align={'center'}
            justify={'center'}
            onPress={() => selecionarImagem(medicamento.nome)}
          >
            <H5>✏️ Editar</H5>
          </CTouchableOpacity>

          <CTouchableOpacity
            bkColor={'#215EED'}
            elevation={'0'}
            width={'150px'}
            height={'50px'}
            radius={'100px'}
            mgTop={'0px'}
            mgBtn={'0px'}
            padding={'0px'}
            direction={'column'}
            align={'center'}
            justify={'center'}
            onPress={() => navigation.navigate('Camera')}
          >
            <H5 cor="white">📷 Tirar Foto</H5>
          </CTouchableOpacity>
        </View>
      </View>
    </CView>
  )
}
