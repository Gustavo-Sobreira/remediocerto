import { CView } from "../containers"
import { ChaveValor, H3, H4, H5 } from "../texto"

export const infoMedicamentos = medicamento => {
  return (
    <CView
      bkColor={'transparent'}
      altura={'100%'}
      elevation={'0'}
      radius={'0px'}
      mgTop={'0px'}
      mgBtn={'0px'}
      padding={'0px'}
      direction={'column'}
      align={'start'}
      justify={'center'}
    >
      <H3 cor={medicamento.quantidade >= 5 ? medicamento.cor_fonte : '#F24141'}>
        {medicamento.nome}
      </H3>
      <ChaveValor>
        <H4>Dias de tratamento: </H4>
        <H5>{medicamento.tempo}</H5>
      </ChaveValor>

      <ChaveValor>
        <H4>Quantidade: </H4>
        <H5>{medicamento.quantidade}</H5>
      </ChaveValor>

      <ChaveValor>
        <H4>Horários: </H4>
        <H5>{medicamento.manha}</H5>
        <H5>{medicamento.tarde}</H5>
        <H5>{medicamento.noite}</H5>
      </ChaveValor>
    </CView>
  )
}
