import React, { useEffect, useState } from 'react'
import MedicamentoRepository from './../../database/sqlite/MedicamentoRepository'
import { Circulo } from './../circulo'
import { Alerta } from './../alerta/Alerta'
import { H4 } from '../texto'
import { infoMedicamentos } from './infoMedicamentos'
import { detalhesMedicamento } from './detalheMedicamento'
import * as ImagePicker from 'expo-image-picker'
import { CTouchableOpacity, CView, CViewScrollScreen } from '../containers'

export const ControleMedicacao = ({ nav: navigation }) => {
  const [medicamentos, setMedicamentos] = useState([])
  const [cardExpandido, setCardExpandido] = useState(null)
  const [cameraPermissao, setCameraPermissao] = useState(null) // Adiciona o estado para a permissão da câmera
  const medicamentoRepo = MedicamentoRepository.getInstance()

  useEffect(() => {
    const listarMedicamentos = async () => {
      const lista = await medicamentoRepo.listarMedicamentos()
      console.log(lista)
      if (lista.length === 0) {
        medicamentoRepo.up()
        medicamentoRepo.criarMedicacao()
      }
      setMedicamentos(lista)

      const algumMedicamentoComPoucasUnidades = lista.some(
        medicamento => medicamento.quantidade < 5
      )
      const alerta = Alerta.getInstance()
      alerta.setEstado(algumMedicamentoComPoucasUnidades)
    }

    listarMedicamentos()
  }, [cardExpandido])

  const expandirCard = index => {
    setCardExpandido(index === cardExpandido ? null : index)
  }

  useEffect(() => {
    const verificarPermissoes = async () => {
      const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync()
      setCameraPermissao(status === 'granted')
    }
    verificarPermissoes()
  }, [])

  return (
    <CViewScrollScreen>
      {Alerta.getInstance().getEstado() && (
        <CView
          bkColor={'red'}
          width={'100%'}
          padding={'5px'}
          direction={'row'}
          justify={'center'}
          align={'center'}
        >
          <H4 cor={'white'}>
            Atenção: Um ou mais medicamentos estão com quantidade inferior a 5 unidades!
          </H4>
        </CView>
      )}

      {medicamentos.map((medicamento, index) => (
        <CTouchableOpacity
          direction={cardExpandido === index ? 'column' : 'row'}
          height={cardExpandido === index ? '320px' : '180px'}
          width="95%"
          key={index}
          mg={'2.5%'}
          bkColor={medicamento.cor_card || 'rgba(191, 59, 156, 0.3)'}
          onPress={() => expandirCard(index)}
        >
          <Circulo bkColor={medicamento.cor_aviso || 'rgba(191, 59, 156, 1)'} />
          <CView
            altura={'100%'}
            elevation={'0'}
            padding={'0px'}
            radius={'0px'}
            mgTop={cardExpandido === index ? '0px' : '15px'}
            bkColor={'transparent'}
            direction={'column'}
            align={'start'}
            justify={'space-between'}
          >
            {cardExpandido === index
              ? detalhesMedicamento({ medicamento, navigation })
              : infoMedicamentos(medicamento)}
          </CView>
        </CTouchableOpacity>
      ))}
    </CViewScrollScreen>
  )
}
