import { Text } from "react-native";
import styled from "styled-components";

export const H1 = styled(Text)`

     color: ${(props) => props.cor || "#000"};
     font-size: ${(props) => props.size || "32px"};
     font-weight: ${(props) => props.weigth || "600"};
     font-family: ${(props) => props.family || "Poppins_500Medium"};
     text-transform: ${(props) => props.tipo || "none"};
`;
