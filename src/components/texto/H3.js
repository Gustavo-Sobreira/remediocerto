import { Text } from "react-native";
import styled from "styled-components";

export const H3 = styled(Text)`

     color: ${(props) => props.cor || "#5C5E71"};
     font-size: ${(props) => props.size || "32px"};
     font-weight: ${(props) => props.weigth || "600"};
     font-family: ${(props) => props.family || "LilitaOne_400Regular"};
     text-transform: ${(props) => props.tipo || "none"};
`;
