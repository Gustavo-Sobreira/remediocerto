import { Text } from "react-native";
import styled from "styled-components";

export const H4 = styled(Text)`

     color: ${(props) => props.cor || "#65687D"};
     font-size: ${(props) => props.size || "22px"};
     font-weight: ${(props) => props.weigth || "300"};
     font-family: ${(props) => props.family || "LilitaOne_400Regular"};
     text-transform: ${(props) => props.tipo || "none"};
`;
