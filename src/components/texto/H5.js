import { Text } from 'react-native'
import styled from 'styled-components'

export const H5 = styled(Text)`
  color: ${props => props.cor || '#8689A6'};
  font-size: ${props => props.size || '22px'};
  font-weight: ${props => props.weigth || '600'};
  font-family: ${props => props.family || 'LilitaOne_400Regular'};
  text-transform: ${props => props.tipo || 'none'};
`
