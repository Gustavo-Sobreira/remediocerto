import { TouchableOpacity, View } from 'react-native'
import styled from 'styled-components'

export const Circulo_Style = styled(TouchableOpacity)`
     width: ${props => props.size || '50px'};
     height: ${props => props.size || '50px'};
     border-radius: ${props => props.radius || '100px'};
     background-color: ${props => props.bkColor || 'rgba(255, 0, 0, 0.2);'};
     margin-right: ${props => props.mgRight || '15px'};

     flex-direction: ${props => props.direction || 'row'};
     justify-content: ${props => props.justify || 'center'};
     align-items: ${props => props.align || 'center'};
     align-content: ${props => props.align || 'center'};
`

export const CirculoBtn = styled(TouchableOpacity)`
     width: ${props => props.size || '50px'};
     height: ${props => props.size || '50px'};
     border-radius: ${props => props.radius || '100px'};
     background-color: ${props => props.bkColor || 'red'};
     margin-right: ${props => props.mgRight || '15px'};

     flex-direction: ${props => props.direction || 'row'};
     justify-content: ${props => props.justify || 'center'};
     align-items: ${props => props.align || 'center'};
     align-content: ${props => props.align || 'center'};
`
