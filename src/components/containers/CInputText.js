import { TextInput} from "react-native";
import styled from "styled-components";


export const CInputText = styled(TextInput)`
     width: ${(props) => props.width || "100%"};
     height: ${(props) => props.height || "100%"};

     padding: ${(props) => props.padding || "5px"};
     margin-top: ${(props) => props.mgTop || "0px"};
     margin-left: ${(props) => props.mgLetf || "0px"};
     margin-right: ${(props) => props.mgRigth || "0px"};
     margin-bottom: ${(props) => props.mgBottom || "0px"};

     border-radius: ${(props) => props.radius || "25px"};
     background-color: ${(props) => props.bkColor || "white"};

     align-items: ${(props) => props.align || "center"};
     align-content: ${(props) => props.align || "center"};
     flex-direction: ${(props) => props.direction || "row"};
     justify-content: ${(props) => props.justify || "start"};
`;