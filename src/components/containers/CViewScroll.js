import { ScrollView } from "react-native";
import styled from "styled-components";


export const CViewScroll = styled(ScrollView)`
  width: ${(props) => props.width || "100%"};
  height: ${(props) => props.height || "100%"};

  padding: ${(props) => props.padding || "15px"};
  margin-top: ${(props) => props.mgTop || "0px"};
  margin-left: ${(props) => props.mgLetf || "0px"};
  margin-right: ${(props) => props.mgRigth || "0px"};
  margin-bottom: ${(props) => props.mgBottom || "0px"};

  border-radius: ${(props) => props.radius || "25px"};
  background-color: ${(props) => props.bkColor || "white"};

  align-content: ${(props) => props.align || "center"};
  flex-direction: ${(props) => props.direction || "row"};
`;
