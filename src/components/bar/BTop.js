import { StatusBar } from "react-native";
import { H1 } from "../texto";
import { CView } from "../containers";

export const BTop = ({
     bkColor,
     fontColor,
     titulo,
     height,
     width,
     elevation
}) => {
     return (
     <CView
          bkColor={bkColor}
          justify={"center"}
          radius={"0px"}
          mgTop={"0px"}
          height={height}
          width={width}
          elevation={elevation}
     >
          <H1
          cor={fontColor}
          tipo={"uppercase"}
          family={"LilitaOne_400Regular"}
          >
          {titulo}
          </H1>
     </CView>
     );
};
