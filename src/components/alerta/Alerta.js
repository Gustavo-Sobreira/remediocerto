export class Alerta {
     static instancia = null;

     estado = false;

     static getInstance() {
          if (!Alerta.instancia) { 
               Alerta.instancia = new Alerta();
          }
          return Alerta.instancia;
     }

     async getEstado(){
          return this.estado;
     }

     async setEstado(nvEstado){
          this.estado = nvEstado;
     }
}
