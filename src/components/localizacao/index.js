import React, { useEffect, useState, useRef } from 'react';
import { CView, CViewMap, CTouchableOpacity } from '../containers';
import { 
  LocationAccuracy,
  getCurrentPositionAsync, 
  requestForegroundPermissionsAsync,
  watchPositionAsync,
} from 'expo-location';
import { Marker } from 'react-native-maps';
import { Image, Alert, Vibration } from 'react-native';


import { LocalizacaoRepository } from '../../database/sqlite/LocalizacaoRepository';
import { Visitas } from './../../database/sqlite/Visitas'
import { Alerta } from './../alerta/Alerta';
import { H5 } from '../texto';

const db = new LocalizacaoRepository();
const visita = new Visitas();

export const Localizacao = () => {
  const [location, setLocation] = useState(null);
  const [farmacia, setFarmacia] = useState([]);
  const [visitados, setVisitados] = useState([]);
  const [alerta, setAlerta] = useState(false); 

  const mapRef = useRef(null);

  async function pedirPermissaoDeLocalizacao(){
    const { granted } = await requestForegroundPermissionsAsync();

    if(granted){
      const posicaoDoUsuario = await getCurrentPositionAsync();
      setLocation(posicaoDoUsuario);
    }
  }

  useEffect(() =>{
    async function obterFarmacias() {
      try {
        const lista = await db.listarLocais();
        setFarmacia(lista);
      } catch (error) {
        db.criarFarmacias();
        console.error("Erro ao obter farmácias: ", error);
      }
    }

    obterFarmacias();

    watchPositionAsync({
      accuracy: LocationAccuracy.Highest,
      timeInterval: 30000,
      distanceInterval: 10
    }, (nvLoc)=>{
      setLocation(nvLoc);
      verificarProximidadeFarmacia(nvLoc);
    });

    pedirPermissaoDeLocalizacao();
  },[]);

  const centralizarMapa = () => {
    mapRef.current?.animateCamera({
      pitch: 70,
      center: location.coords
    });
  }

  const calcularDistancia = (ponto1, ponto2) => {
    const { latitude: lat1, longitude: lon1 } = ponto1.coords;
    const { latitude: lat2, longitude: lon2 } = ponto2;
    const raioTeraa = 6371e3; 
    const latRad = lat1 * Math.PI / 180; 
    const latRad2 = lat2 * Math.PI / 180;
    const deltaLat = (lat2-lat1) * Math.PI / 180;
    const deltaLong = (lon2-lon1) * Math.PI / 180;

    const a = Math.sin(deltaLat/2) * Math.sin(deltaLat/2) +
              Math.cos(latRad) * Math.cos(latRad2) *
              Math.sin(deltaLong/2) * Math.sin(deltaLong/2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

    return raioTeraa * c;
  }

  const verificarProximidadeFarmacia = async (posicaoUsuario) => {
    farmacia.forEach(async farmacia => {
      const distancia = calcularDistancia(posicaoUsuario, farmacia);
      // console.log(distancia);
      try {
        console.log("Locais visitados: ", await visita.contarVisitas());
      } catch (error) {
        console.error("Erro ao contar visitas:", error);
      }
      
      if (distancia <= 10 && alerta) { 
        Vibration.vibrate();
        Alert.alert(
          "Alerta de Medicamentos",
          "Você tem poucos medicamentos, há uma farmácia por perto"
        );
        visita.adicionarVisita(farmacia.latitude, farmacia.longitude);
        try {
          console.log("Locais visitados: ", await visita.contarVisitas());
        } catch (error) {
          console.error("Erro ao contar visitas:", error);
        }
      }
    });
  }

  useEffect(() => {
    const alertaClasse = Alerta.getInstance();
    alertaClasse.getEstado().then((estado) => setAlerta(estado));
  }, []);

  return (
      <CView height='100%' padding='0px'>
        {
          location && 
            <CViewMap
              ref={mapRef}
              initialRegion={{
                latitude: location.coords.latitude,
                longitude: location.coords.longitude,
                latitudeDelta: 0.005,
                longitudeDelta: 0.005
              }}
            >
              <Marker
                title='Eu'
                description='Minha localização atual'
                pinColor="#38a450"
                coordinate={{
                  latitude: location.coords.latitude,
                  longitude: location.coords.longitude,
                }}
              >
                <Image
                  source={require('../../../assets/here.png')}
                  style={{width:40, height:40}}
                />
              </Marker>

              {farmacia.map(farmacia => (
                <Marker
                  key={farmacia.id}
                  title={farmacia.nome}
                  description={"Atendimento: " + farmacia.atendimento}
                  coordinate={{
                    latitude: farmacia.latitude,
                    longitude: farmacia.longitude
                  }}
                >
                  <Image
                    source={require('../../../assets/pharmacy.png')}
                    style={{ width: 40, height: 40 }}
                  />
                </Marker>
              ))}
            </CViewMap>
        }
        <CTouchableOpacity 
          position={"absolute"} 
          width={"60px"} 
          height={"60px"}
          radius={"100px"}
          bkColor={"#10a194"}
          onPress={() => centralizarMapa()}
          >
          <Image
            source={require('../../../assets/gps.png')}
            style={{ width: 50, height: 50 }}
          />
        </CTouchableOpacity>
      </CView>
  );
}
