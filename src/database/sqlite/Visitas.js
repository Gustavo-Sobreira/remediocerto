import { executarTransacao } from './SqliteRepository'

let instancia = null

export class Visitas {
  NOME_TABELA = 'visitas'

  constructor() {
    if (!instancia) {
      instancia = this
      this.up()
    }
    return instancia
  }

  async up() {
    // await executarTransacao(`DROP TABLE ${this.NOME_TABELA};`);

    await executarTransacao(`
               CREATE TABLE IF NOT EXISTS ${this.NOME_TABELA} (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    latitude REAL, 
                    longitude REAL
               );
          `)
  }

  async deletarTudo() {
    await executarTransacao(`DROP TABLE ${this.NOME_TABELA};`)
  }

  async contarVisitas() {
    try {
      const result = await executarTransacao(`
                    SELECT latitude, longitude, COUNT(*) as visitas 
                    FROM ${this.NOME_TABELA} 
                    GROUP BY latitude, longitude;
               `)
      return result.rows._array
    } catch (error) {
      console.error('Erro ao contar visitas:', error)
      return []
    }
  }

  async adicionarVisita(latitude, longitude) {
    try {
      await executarTransacao(
        `
                    INSERT INTO ${this.NOME_TABELA} (latitude, longitude) 
                    VALUES (?, ?);
               `,
        [latitude, longitude]
      )
    } catch (error) {
      console.error('Erro ao adicionar visita:', error)
    }
  }
}
