import { executarTransacao } from './SqliteRepository'

export class LocalizacaoRepository {
  NOME_TABELA = 'local'

  constructor() {
    this.up()
  }

  async up() {
    // await executarTransacao(`DROP TABLE ${this.NOME_TABELA};`);

    await executarTransacao(`
               CREATE TABLE IF NOT EXISTS ${this.NOME_TABELA} (
                    id INTEGER PRIMARY KEY AUTOINCREMENT, 
                    nome TEXT, 
                    latitude REAL, 
                    longitude REAL
               );
          `)
  }

  async criarFarmacias() {
    console.log('-->', this.listarLocais.length)
    if (this.listarLocais.length === this.farmacias.length) {
      for (const farmacia of this.farmacias) {
        await executarTransacao(
          `
                         INSERT INTO ${this.NOME_TABELA} 
                         (nome, latitude, longitude) 
                         VALUES (?, ?, ?);
                    `,
          [farmacia.nome, farmacia.latitude, farmacia.longitude]
        )
      }
    }
  }

  async deletarTudo() {
    await executarTransacao(`DROP TABLE ${this.NOME_TABELA};`)
  }

  async listarLocais() {
    try {
      const result = await executarTransacao(`SELECT * FROM ${this.NOME_TABELA};`)
      return result.rows._array.map(local => ({
        id: local.id,
        nome: local.nome,
        latitude: parseFloat(local.latitude),
        longitude: parseFloat(local.longitude),
        atendimento: local.atendimento
      }))
    } catch (error) {
      console.error('Erro ao listar locais:', error)
      return []
    }
  }

  async adicionarLocal(nome, latitude, longitude) {
    try {
      const result = await executarTransacao(
        `
                    INSERT INTO ${this.NOME_TABELA} 
                    (nome, latitude, longitude) 
                    VALUES (?, ?, ?);
               `,
        [nome, latitude, longitude]
      )
      return result.insertId
    } catch (error) {
      console.error('Erro ao adicionar local:', error)
      return null
    }
  }

  farmacias = [
    {
      latitude: -21.76183889417198,
      longitude: -43.34936069454236,
      nome: 'Drogaria Souza',
      atendimento: '24hr'
    },
    {
      latitude: -21.76135710777179,
      longitude: -43.349365205462114,
      nome: 'Drogaria Pacheco',
      atendimento: '24hr'
    },
    {
      latitude: -21.761977145616456,
      longitude: -43.34839084677622,
      nome: 'Manipulação Arcanjo Miguel',
      atendimento: '24hr'
    },
    {
      latitude: -21.761126687610883,
      longitude: -43.34871112207805,
      nome: 'Drogaria Souza',
      atendimento: '24hr'
    },
    {
      latitude: -21.760984245875893,
      longitude: -43.34904493013911,
      nome: 'Drogaria Araujo',
      atendimento: '24hr'
    },
    {
      latitude: -21.760623951436184,
      longitude: -43.34964939338789,
      nome: 'Drogaria Serve Mais',
      atendimento: '24hr'
    },
    {
      latitude: -21.76031726038667,
      longitude: -43.34961678842428,
      nome: 'Farmacia São Mateus',
      atendimento: '24hr'
    },
    {
      latitude: -21.759261258187006,
      longitude: -43.351200468529036,
      nome: 'Drogaria Mais',
      atendimento: '24hr'
    },
    {
      latitude: -21.759201368051922,
      longitude: -43.35041906597735,
      nome: 'Drogaria JR',
      atendimento: '24hr'
    },
    {
      latitude: -21.758704631265445,
      longitude: -43.348621081439056,
      nome: 'Drogaria Araujo',
      atendimento: '24hr'
    },
    {
      latitude: -21.666869290001827,
      longitude: -43.305061649284795,
      nome: 'Drogaria Barateira',
      atendimento: '24hr'
    },
    {
      latitude: -21.737074895053357,
      longitude: -43.40413696071508,
      nome: 'Droga Mais',
      atendimento: '24hr'
    }
  ]
}
