import { executarTransacao } from './SqliteRepository';

class MedicamentoRepository {

  NOME_TABELA = 'medicamento';

  async up() {
    await executarTransacao(`
      CREATE TABLE IF NOT EXISTS ${this.NOME_TABELA} (
        id INTEGER PRIMARY KEY AUTOINCREMENT, 
        nome TEXT, 
        quantidade INT, 
        tempo INT, 
        finalidade TEXT, 
        manha INT, 
        tarde INT, 
        noite INT, 
        cor_aviso TEXT, 
        cor_card TEXT, 
        cor_fonte TEXT,
        uri TEXT
      );
    `);
  }
  
  async criarMedicacao() {
    for (const medicamento of this.medicamentos()) {
      await executarTransacao(
        `
          INSERT INTO ${this.NOME_TABELA} 
          (nome, quantidade, tempo, finalidade, manha, tarde, noite, cor_aviso, cor_card, cor_fonte, uri) 
          VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
        `,
        [
          medicamento.nome,
          medicamento.quantidade,
          medicamento.tempo,
          medicamento.finalidade,
          medicamento.manha,
          medicamento.tarde,
          medicamento.noite,
          medicamento.cor_aviso,
          medicamento.cor_card,
          medicamento.cor_fonte,
          medicamento.uri
        ]
      );
    }
  }

  async deletarTudo() {
    await executarTransacao(`DROP TABLE ${this.NOME_TABELA};`);
  }

  async listarMedicamentos() {
    try {
      const result = await executarTransacao(`SELECT * FROM ${this.NOME_TABELA};`);

      return result.rows._array.map(medicamento => ({
        id: medicamento.id,
        nome: medicamento.nome,
        quantidade: medicamento.quantidade,
        tempo: medicamento.tempo,
        finalidade: medicamento.finalidade,
        manha: medicamento.manha,
        tarde: medicamento.tarde,
        noite: medicamento.noite,
        cor_aviso: medicamento.cor_aviso,
        cor_card: medicamento.cor_card,
        cor_fonte: medicamento.cor_fonte,
        uri: medicamento.uri
      }));
    } catch (error) {
      console.error('Erro ao listar medicamentos:', error);
      return [];
    }
  }

  async adicionarMedicamento(medicamento) {
    try {
      const {
        nome,
        quantidade,
        tempo,
        finalidade,
        manha,
        tarde,
        noite,
        cor_aviso,
        cor_card,
        cor_fonte,
        uri
      } = medicamento;
      const result = await executarTransacao(
        `
          INSERT INTO ${this.NOME_TABELA} 
          (nome, quantidade, tempo, finalidade, manha, tarde, noite, cor_aviso, cor_card, cor_fonte, uri) 
          VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
        `,
        [nome, quantidade, tempo, finalidade, manha, tarde, noite, cor_aviso, cor_card, cor_fonte, uri]
      );
      return result.insertId;
    } catch (error) {
      console.error('Erro ao adicionar medicamento:', error);
      return null;
    }
  }

  async editarMedicamento(nome, uri) {
    try {
      const result = await executarTransacao(
        `
          UPDATE ${this.NOME_TABELA} 
          SET uri=? 
          WHERE nome=?
        `,
        [uri, nome]
      );
      console.log('[200] - URI substituida com sucesso.');
      return result;
    } catch (error) {
      console.error('Erro ao editar medicamento:', error);
      return null;
    }
  }
  
  medicamentos() {
    return [
      {
        nome: 'Loratadina',
        quantidade: 20,
        tempo: 5,
        finalidade: 'Alergia',
        manha: 1,
        tarde: 0,
        noite: 1,
        cor_aviso: '#F24141',
        cor_card: '#f241414d',
        cor_fonte: '#FFf',
        uri: 'Uri Invalida'
      },
      {
        nome: 'Losartana',
        quantidade: 5,
        tempo: 5,
        finalidade: 'Pressão Alta',
        manha: 1,
        tarde: 1,
        noite: 2,
        cor_aviso: '#F28322',
        cor_card: '#f283224d',
        cor_fonte: '#fff',
        uri: 'Uri Invalida'
      },
      {
        nome: 'Neosaldina',
        quantidade: 6,
        tempo: 0,
        finalidade: 'Dor de cabeça',
        manha: 0,
        tarde: 0,
        noite: 0,
        cor_aviso: '#62B7D9',
        cor_card: '#62b7d94d',
        cor_fonte: '#fff',
        uri: 'Uri Invalida'
      },
      {
        nome: 'Glibenclamida',
        quantidade: 2,
        tempo: 0,
        finalidade: 'Diabetes',
        manha: 1,
        tarde: 0,
        noite: 0,
        cor_aviso: '#5BA698',
        cor_card: '#5ba6984d',
        cor_fonte: '#fff',
        uri: 'Uri Invalida'
      }
    ];
  }
}

class Singleton {
  constructor() {
    throw new Error('Use Singleton.getInstance()');
  }

  static getInstance() {
    if (!Singleton.instance) {
      Singleton.instance = new MedicamentoRepository('medicamento');
    }
    return Singleton.instance;
  }
}

export default Singleton;
