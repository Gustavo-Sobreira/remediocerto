import * as SQLite from 'expo-sqlite'

export const db = SQLite.openDatabase('database.sqlite')

export const executarTransacao = (sql, values) => {
  return new Promise((resolve, reject) => {
    db.transaction(tx => {
      tx.executeSql(
        sql,
        values,
        (_, resultSet) => {
          resolve(resultSet)
        },
        (_, error) => {
          reject(error)
          return true
        }
      )
    })
  })
}
