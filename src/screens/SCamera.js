import React, { useState, useEffect } from 'react';
import { View, Button, StyleSheet, Text } from 'react-native';
import { Camera } from 'expo-camera';
import { MaterialCommunityIcons } from '@expo/vector-icons'; // Importa o ícone MaterialIcons
import * as MediaLibrary from 'expo-media-library';
import { CTouchableOpacity } from '../components/containers';

export default function SCamera({ navigation }) {
  const [permissao, setPermissao] = useState(null);
  const [cameraRef, setCameraRef] = useState(null);
  const [cameraLigada, setCameraLigada] = useState(true);

  useEffect(() => {
    (async () => {
      const { status } = await Camera.requestCameraPermissionsAsync();
      setPermissao(status === 'granted');
      console.log('Permissão da câmera solicitada');
    })();
  }, []);

  const toggleCamera = () => {
    setCameraLigada(!cameraLigada);
  };

  const takePicture = async () => {
    if (cameraRef) {
      const foto = await cameraRef.takePictureAsync();
      savePhoto(foto.uri); 
    }
  };

  const savePhoto = async (uri) => {
    try {
      await MediaLibrary.saveToLibraryAsync(uri);
      console.log('Foto salva na galeria!');
      navigation.navigate('Home');
    } catch (error) {
      console.error('Erro ao salvar foto na galeria:', error);
    }
  };

  if (permissao === null) {
    return <View />;
  }
  if (permissao === false) {
    return <View><Text>Sem acesso à câmera</Text></View>;
  }

  return (
    <View style={{ flex: 1 }}>
      {cameraLigada && (
        <Camera
          style={{ flex: 1 }}
          type={Camera.Constants.Type.back}
          ref={(ref) => setCameraRef(ref)}
        />
      )}
      <View style={styles.buttonContainer}>
        {cameraLigada && (
          <View style={styles.captureButtonContainer}>
            <Button
              title="Tira foto"
              onPress={takePicture}
              buttonStyle={styles.captureButton}
            />
          </View>
        )}
        <Button
          title={cameraLigada ? 'Desligar Câmera' : 'Ligar Câmera'}
          onPress={toggleCamera}
          color={cameraLigada ? '#FF5733' : '#45B39D'} // Cor do botão dependendo do estado da câmera
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  buttonContainer: {
    position: 'absolute',
    bottom: 20,
    left: 0,
    right: 0,
    alignItems: 'center',
  },
  captureButtonContainer: {
    marginTop: 10,
    borderRadius: 50,
    backgroundColor: '#FF5733', // Cor do botão de captura
  },
  captureButton: {
    width: 50,
    height: 50,

  },
});
