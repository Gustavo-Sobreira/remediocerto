import React, { useEffect, useState } from 'react';
import { View, ScrollView, Dimensions } from 'react-native';
import { BarChart, PieChart } from 'react-native-chart-kit';
import { BTop } from './../components/bar';
import Singleton from '../database/sqlite/MedicamentoRepository';

const CorPrimaria = '#388CA6';

export const SGrafico = () => {
  const [medicamentos, setMedicamentos] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const repository = Singleton.getInstance();
      const data = await repository.listarMedicamentos();
      setMedicamentos(data);
    };

    fetchData();
  }, []);

  const nomes = medicamentos.map(m => m.nome);
  const quantidades = medicamentos.map(m => m.quantidade);

  return (
    <ScrollView>
      <BTop bkColor={CorPrimaria} fontColor={'#fff'} titulo={'Estoque'} />
      <View style={{ padding: 20 }}>
        <BarChart
          data={{
            labels: nomes,
            datasets: [
              {
                data: quantidades
              }
            ]
          }}
          width={Dimensions.get('window').width - 40}
          height={220}
          yAxisLabel=""
          chartConfig={{
            backgroundColor: '#e26a00',
            backgroundGradientFrom: '#fb8c00',
            backgroundGradientTo: '#ffa726',
            decimalPlaces: 2,
            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            style: {
              borderRadius: 16
            },
            propsForDots: {
              r: '6',
              strokeWidth: '2',
              stroke: '#ffa726'
            }
          }}
          style={{
            marginVertical: 8,
            borderRadius: 16
          }}
        />

        <PieChart
          data={quantidades.map((value, index) => ({
            name: nomes[index],
            quantity: value,
            color: `#${Math.floor(Math.random() * 16777215).toString(16)}` // Gera cores aleatórias
          }))}
          width={Dimensions.get('window').width - 40}
          height={220}
          chartConfig={{
            backgroundColor: '#e26a00',
            backgroundGradientFrom: '#fb8c00',
            backgroundGradientTo: '#ffa726',
            decimalPlaces: 2,
            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            style: {
              borderRadius: 16
            }
          }}
          accessor="quantity"
          backgroundColor="transparent"
          paddingLeft="15"
          absolute
        />
      </View>
    </ScrollView>
  );
};
