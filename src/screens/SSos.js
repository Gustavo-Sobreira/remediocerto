import { BTop } from './../components/bar'

const CorPrimaria = '#FF7373'
const CorMonocromatica = '#B35050'

export const SSos = () => {
  return <BTop bkColor={CorPrimaria} fontColor={'#fff'} titulo={'Emergência'}></BTop>
}
