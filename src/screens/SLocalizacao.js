import { BTop } from '../components/bar'
import { CViewScreen, CViewScroll } from '../components/containers'
import { Localizacao } from '../components/localizacao'

const CorPrimaria = '#7138A6'
const CorMonoCromatica = '#5E3E7B'

export const SLocalizacao = () => {
  return (
    <CViewScreen>
      <BTop bkColor={CorPrimaria} fontColor={'#fff'} titulo={'Localizacao'} />
      <Localizacao></Localizacao>
    </CViewScreen>
  )
}
