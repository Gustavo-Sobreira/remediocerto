import { CViewScreen } from '../components/containers'
import { ControleMedicacao } from '../components/medicamento/ControleMedicacao'
import { BTop } from './../components/bar'

const CorPrimaria = '#38A69D'
const CorMonoCromatica = '#3E7B76'

export const SHome = ({navigation}) => {
  return (
    <CViewScreen>
      <BTop bkColor={CorPrimaria} fontColor={'#fff'} titulo={'Medicamentos'} />
      <ControleMedicacao nav={navigation} ></ControleMedicacao>
    </CViewScreen>
  )
}
