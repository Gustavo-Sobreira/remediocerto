import { CViewScreen } from '../components/containers'
import { BTop } from './../components/bar'

const CorPrimaria = '#4D93BF'
const CorMonocromatica = '#2E5873'

export const SPerfil = () => {
  return (
    <CViewScreen>
      <BTop bkColor={CorPrimaria} fontColor={'#fff'} titulo={'Perfil'}/>
    </CViewScreen>
  )  
}
